import gql from 'graphql-tag';

const storyFragment = gql`
  fragment StoryFragment on story {
    id
    title
    user {
      id
      name
      email
    }
    created_at
  }
`;

export default storyFragment;
