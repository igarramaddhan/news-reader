import {HttpLink} from 'apollo-link-http';
import {ApolloClient} from 'apollo-client';
import {InMemoryCache} from 'apollo-cache-inmemory';

const cache = new InMemoryCache();

const httpLink = new HttpLink({
  uri: 'https://news-mock-api.herokuapp.com/v1/graphql',
});

const client = new ApolloClient({
  cache,
  link: httpLink,
  resolvers: {},
});

export default client;
